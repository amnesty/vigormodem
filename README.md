# Vigor Modem Telnet Script
Connects to your Vigor Modem and issues a "vdsl status" command via Telnet.
All interesting values are inserted into a MySQL Database for later use with any graphing tool. (like Grafana)

A Sample Grafana Dashboard JSON can be used. Datasource name is "MySQL"
![grafana dashboard](grafana.PNG)