#!/bin/bash
file="/opt/modemstatus/current_status"

#Get current modem status via telnet
/opt/modemstatus/get.expect > /opt/modemstatus/current_status

ds_attainable=$(grep Attainable $file | awk '{print $4}' | tr -d ":")
us_attainable=$(grep Attainable $file | awk '{print $10}' )
ds_actual=$(grep Actual $file | awk '{print $4}' | tr -d ":")
us_actual=$(grep Actual $file | awk '{print $10}')
ds_db=$(grep PSD $file | awk '{print $5,$6}' | tr -d " ")
us_db=$(grep PSD $file | awk '{print $12}' | tr -d " ")
crc_modem=$(grep CRC $file | awk '{print $5}')
crc_dslam=$(grep CRC $file | awk '{print $10}' | tr -d "\r")
snr_modem=$(grep "Cur SNR Margin" $file | awk '{print $11}')
snr_dslam=$(grep "Far SNR Margin" $file | awk '{print $11}')

#US dB value can be negative
# Vigor sends a value like "-1.-7". double minus needs to be removed
if [[ $us_db = \-* ]]; then
	us_db=$(echo $us_db | tr -d "-")
	us_db="-$us_db"
fi

echo "INSERT INTO modemstatus.values (ds_attainable, us_attainable, ds_actual, us_actual, ds_db, us_db, crc_modem, crc_dslam, snr_modem, snr_dslam) VALUES ('$ds_attainable', '$us_attainable', '$ds_actual', '$us_actual', '$ds_db', '$us_db', '$crc_modem', '$crc_dslam', '$snr_modem', '$snr_dslam');" | /opt/modemstatus/mysql
